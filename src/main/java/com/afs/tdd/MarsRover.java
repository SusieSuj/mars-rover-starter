package com.afs.tdd;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MarsRover {
    private Location location;


    public MarsRover(Location location) {
        this.location = location;
    }

    public Location executeCommand(Command command) {
        if (command == null)
            return null;
        if (command.equals(Command.Move)) {
            moveForward();
        } else if (command.equals(Command.Left)) {
            moveLeft();
        } else if (command.equals(Command.Right)) {
            moveRight();
        }
        return location;
    }

    public void moveForward() {
        if (location.getDirection() == Direction.North) {
            location.setLocationY(location.getLocationY() + 1);
        } else if (location.getDirection() == Direction.South) {
            location.setLocationY(location.getLocationY() - 1);
        } else if (location.getDirection() == Direction.East) {
            location.setLocationX(location.getLocationX() + 1);
        } else if (location.getDirection() == Direction.West) {
            location.setLocationX(location.getLocationX() - 1);
        }
    }

    public void moveLeft() {
        int directionIndex = location.getDirection().ordinal();
        int afterMoveDirectionIndex = directionIndex == 0 ? 3 : directionIndex - 1;
        location.setDirection(Direction.class.getEnumConstants()[afterMoveDirectionIndex]);
    }

    public void moveRight() {
        int directionIndex = location.getDirection().ordinal();
        int afterMoveDirectionIndex = directionIndex == 3 ? 0 : directionIndex + 1;
        location.setDirection(Direction.class.getEnumConstants()[afterMoveDirectionIndex]);
    }

    public Location executeCommands(String commands) {
        List<String> conmmandList = Stream.iterate(0,n->++n).limit((commands.length()))
                .map(n->""+commands.charAt(n))
                .collect(Collectors.toList());
        for (int i = 0; i < conmmandList.size(); i++) {
            switch (conmmandList.get(i)){
                case "M":
                    moveForward();
                    break;
                case "L":
                    moveLeft();
                    break;
                case "R":
                    moveRight();
                    break;
            }
        }
        return location;
    }
}

