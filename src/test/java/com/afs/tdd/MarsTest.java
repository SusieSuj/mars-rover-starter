package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MarsTest {
    //    00N-M-O1N
    @Test
    void should_change_to_0_1_North_when_executeCommand_given_location_0_0_N_and_move() {
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = Command.Move;
        Location locationAfterMove = marsRover.executeCommand(command);
        Assertions.assertEquals(0, locationAfterMove.getLocationX());
        Assertions.assertEquals(1, locationAfterMove.getLocationY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }

    //    00N-L-00W
    @Test
    void should_change_to_0_0_West_when_executeCommand_given_location_0_0_N_and_left() {
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = Command.Left;
        Location locationAfterMove = marsRover.executeCommand(command);
        Assertions.assertEquals(0, locationAfterMove.getLocationX());
        Assertions.assertEquals(0, locationAfterMove.getLocationY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }

    //    00N-R-00E
    @Test
    void should_change_to_0_0_East_when_executeCommand_given_location_0_0_N_and_right() {
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = Command.Right;
        Location locationAfterMove = marsRover.executeCommand(command);
        Assertions.assertEquals(0, locationAfterMove.getLocationX());
        Assertions.assertEquals(0, locationAfterMove.getLocationY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());
    }


    //    00S-M- 0 -1 S
    @Test
    void should_change_to_0_negative_1_South_when_executeCommand_given_location_0_0_S_and_move() {
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = Command.Move;
        Location locationAfterMove = marsRover.executeCommand(command);
        Assertions.assertEquals(0, locationAfterMove.getLocationX());
        Assertions.assertEquals(-1, locationAfterMove.getLocationY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }


    //    00S-L- 00E
    @Test
    void should_change_to_0_0_East_when_executeCommand_given_location_0_0_S_and_left() {
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = Command.Left;
        Location locationAfterMove = marsRover.executeCommand(command);
        Assertions.assertEquals(0, locationAfterMove.getLocationX());
        Assertions.assertEquals(0, locationAfterMove.getLocationY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());
    }

    //    00S-R- 00w
    @Test
    void should_change_to_0_0_West_when_executeCommand_given_location_0_0_S_and_right() {
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = Command.Right;
        Location locationAfterMove = marsRover.executeCommand(command);
        Assertions.assertEquals(0, locationAfterMove.getLocationX());
        Assertions.assertEquals(0, locationAfterMove.getLocationY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }

    //    00E-M-10E
    @Test
    void should_change_to_1_0_East_when_executeCommand_given_location_0_0_E_and_move() {
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = Command.Move;
        Location locationAfterMove = marsRover.executeCommand(command);
        Assertions.assertEquals(1, locationAfterMove.getLocationX());
        Assertions.assertEquals(0, locationAfterMove.getLocationY());
        Assertions.assertEquals(Direction.East, locationAfterMove.getDirection());
    }

    //    00E-L-00N
    @Test
    void should_change_to_0_0_North_when_executeCommand_given_location_0_0_E_and_left() {
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = Command.Left;
        Location locationAfterMove = marsRover.executeCommand(command);
        Assertions.assertEquals(0, locationAfterMove.getLocationX());
        Assertions.assertEquals(0, locationAfterMove.getLocationY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }

    //    00E-R-00S
    @Test
    void should_change_to_0_0_South_when_executeCommand_given_location_0_0_E_and_right() {
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = Command.Right;
        Location locationAfterMove = marsRover.executeCommand(command);
        Assertions.assertEquals(0, locationAfterMove.getLocationX());
        Assertions.assertEquals(0, locationAfterMove.getLocationY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }

    //    00W-M- -1 0 W
    @Test
    void should_change_to_negative_1_0_West_when_executeCommand_given_location_0_0_W_and_move() {
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = Command.Move;
        Location locationAfterMove = marsRover.executeCommand(command);
        Assertions.assertEquals(-1, locationAfterMove.getLocationX());
        Assertions.assertEquals(0, locationAfterMove.getLocationY());
        Assertions.assertEquals(Direction.West, locationAfterMove.getDirection());
    }

    //    00W-L-00S
    @Test
    void should_change_to_0_0_South_when_executeCommand_given_location_0_0_W_and_left() {
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = Command.Left;
        Location locationAfterMove = marsRover.executeCommand(command);
        Assertions.assertEquals(0, locationAfterMove.getLocationX());
        Assertions.assertEquals(0, locationAfterMove.getLocationY());
        Assertions.assertEquals(Direction.South, locationAfterMove.getDirection());
    }

    //    00W-R-00N
    @Test
    void should_change_to_0_0_North_when_executeCommand_given_location_0_0_W_and_right() {
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command command = Command.Right;
        Location locationAfterMove = marsRover.executeCommand(command);
        Assertions.assertEquals(0, locationAfterMove.getLocationX());
        Assertions.assertEquals(0, locationAfterMove.getLocationY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }


    //MLMR:0,0,N- -1,1,N
    @Test
    void should_change_to_nagetive_1_1_North_when_executeCommand_given_location_0_0_N_move_left_move_right(){
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        String commands = "MLMR";
        Location locationAfterMove = marsRover.executeCommands(commands);
        Assertions.assertEquals(-1, locationAfterMove.getLocationX());
        Assertions.assertEquals(1, locationAfterMove.getLocationY());
        Assertions.assertEquals(Direction.North, locationAfterMove.getDirection());
    }

}
