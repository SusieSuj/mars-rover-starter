# O

- I learned TDD today. 
- In today's code review, I received some useful suggestions from my crew. For example, use the methods recommended by the IDEA tool to improve my code. And I learned how to use IDEA to rewrite the equals() and hashCode().
- TDD means Test-Driven development.  We write test before code. In this process, we will be clearer about the need. And we can find the problem quickly after it appear. 

# R

- I can learn a lot of new things in code review

# I

- Although we wouldn't  git commit everytimes we write a test, we still need to pratice this process. Because the real projects are so complex that it can be serious consequences if bug appear.

# D

- Coding is a very serious thing. There are a lot of details that we need to pay attention to. I should  implement the TDD thinking into my work.